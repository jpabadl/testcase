package com.javiermoreno.bankof.bankoffunctionaltests;

import java.math.BigDecimal;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author ciberado
 */
public class TransaccionIT {
    
    private static WebDriver driver = null;
    
    public TransaccionIT() {
    }
    
    @BeforeClass
    public static void inicializarDriver() {
        driver = new ChromeDriver();
    }
    
    @AfterClass
    public static void liquidarDriver() {
        driver.quit();
    }
    

    @Test
    public void comprobarFlujoCorrectoTransferencia() {
         driver.get("http://automationpractice.com/index.php");
        
        //Envia la cadena carro al input con nombre search_query
        WebElement importeElem = driver.findElement(By.name("search_query"));
        importeElem.sendKeys("carro");
        
        //Click en el boton asociado al input search_query
        WebElement cmdBuscar = driver.findElement(By.name("submit_search"));
        cmdBuscar.click();
        
    }
    
}
